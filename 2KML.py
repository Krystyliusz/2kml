__author__ = 'Krystyliusz'

import csv
global glob
import glob
from math import sqrt

class INS2KML(object):
    def __init__(self):
        pass

    def find_file(self):
        # Metoda wyszukuje plików z rozszerzeniem INS oraz ASC
        # Wynikiem lista plików file_list
        asc = glob.glob('*.asc')
        ins = glob.glob('*.ins')
        file_list = asc + ins
        print('W folderze znajdują się pliki:', file_list, '\n')

        return file_list

    def create_kml(self, file):
        #Przetwarza dane z pliku i na podstawie pozycji z depeszy tworzy plik KML
        intro = '<?xml version="1.0" encoding="UTF-8"?>\n'\
                                '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n'\
                                '<Document>\n'\
                                '	<name>{}.kml</name>\n'\
                                '	<open>1</open>\n'\
                                '	<Style id="Q5">\n'\
                                '		<IconStyle>\n'\
                                '			<color>ffff00ff</color>\n'\
                                '			<scale>0.5</scale>\n'\
                                '			<Icon>\n'\
                                '				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>\n'\
                                '			</Icon>\n'\
                                '		</IconStyle>\n'\
                                '		<LabelStyle>\n'\
                                '			<scale>0</scale>\n'\
                                '		</LabelStyle>\n'\
                                '		<BalloonStyle>\n'\
                                '			<text><![CDATA[<b><font color="#CC0000" size="+3">$[name]</font></b><br>$[description]</font><br/>]]></text>\n'\
                                '			<bgColor>ffd5f3fa</bgColor>\n'\
                                '		</BalloonStyle>\n'\
                                '	</Style>\n'\
                                '	<Style id="Q1">\n'\
                                '		<IconStyle>\n'\
                                '			<color>ff00ff64</color>\n'\
                                '			<scale>0.5</scale>\n'\
                                '			<Icon>\n'\
                                '				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>\n'\
                                '			</Icon>\n'\
                                '		</IconStyle>\n'\
                                '		<LabelStyle>\n'\
                                '			<scale>0</scale>\n'\
                                '		</LabelStyle>\n'\
                                '		<BalloonStyle>\n'\
                                '			<text><![CDATA[<b><font color="#CC0000" size="+3">$[name]</font></b><br>$[description]</font><br/>]]></text>\n'\
                                '			<bgColor>ffd5f3fa</bgColor>\n'\
                                '		</BalloonStyle>\n'\
                                '	</Style>\n'\
                                '	<Style id="Q3">\n'\
                                '		<IconStyle>\n'\
                                '			<color>ffff9600</color>\n'\
                                '			<scale>0.5</scale>\n'\
                                '			<Icon>\n'\
                                '				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>\n'\
                                '			</Icon>\n'\
                                '		</IconStyle>\n'\
                                '		<LabelStyle>\n'\
                                '			<scale>0</scale>\n'\
                                '		</LabelStyle>\n'\
                                '		<BalloonStyle>\n'\
                                '			<text><![CDATA[<b><font color="#CC0000" size="+3">$[name]</font></b><br>$[description]</font><br/>]]></text>\n'\
                                '			<bgColor>ffd5f3fa</bgColor>\n'\
                                '		</BalloonStyle>\n'\
                                '	</Style>\n'\
                                '	<Style id="Q4">\n'\
                                '		<IconStyle>\n'\
                                '			<color>ffff6496</color>\n'\
                                '			<scale>0.5</scale>\n'\
                                '			<Icon>\n'\
                                '				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>\n'\
                                '			</Icon>\n'\
                                '		</IconStyle>\n'\
                                '		<LabelStyle>\n'\
                                '			<scale>0</scale>\n'\
                                '		</LabelStyle>\n'\
                                '		<BalloonStyle>\n'\
                                '			<text><![CDATA[<b><font color="#CC0000" size="+3">$[name]</font></b><br>$[description]</font><br/>]]></text>\n'\
                                '			<bgColor>ffd5f3fa</bgColor>\n'\
                                '		</BalloonStyle>\n'\
                                '	</Style>\n'\
                                '	<Style id="Q6">\n'\
                                '		<IconStyle>\n'\
                                '			<color>ff0000ff</color>\n'\
                                '			<scale>0.5</scale>\n'\
                                '			<Icon>\n'\
                                '				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>\n'\
                                '			</Icon>\n'\
                                '		</IconStyle>\n'\
                                '		<LabelStyle>\n'\
                                '			<scale>0</scale>\n'\
                                '		</LabelStyle>\n'\
                                '		<BalloonStyle>\n'\
                                '			<text><![CDATA[<b><font color="#CC0000" size="+3">$[name]</font></b><br>$[description]</font><br/>]]></text>\n'\
                                '			<bgColor>ffd5f3fa</bgColor>\n'\
                                '		</BalloonStyle>\n'\
                                '	</Style>\n'\
                                '	<Style id="Q2">\n'\
                                '		<IconStyle>\n'\
                                '			<color>ff78c800</color>\n'\
                                '			<scale>0.5</scale>\n'\
                                '			<Icon>\n'\
                                '				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>\n'\
                                '			</Icon>\n'\
                                '		</IconStyle>\n'\
                                '		<LabelStyle>\n'\
                                '			<scale>0</scale>\n'\
                                '		</LabelStyle>\n'\
                                '		<BalloonStyle>\n'\
                                '			<text><![CDATA[<b><font color="#CC0000" size="+3">$[name]</font></b><br>$[description]</font><br/>]]></text>\n'\
                                '			<bgColor>ffd5f3fa</bgColor>\n'\
                                '		</BalloonStyle>\n'\
                                '	</Style>\n'\
                                '	<Folder id="ID4">\n'\
                                '		<name>Epochs</name>\n'
        end = '	</Folder>\n'\
             '</Document>\n'\
             '</kml>\n'

        with open(file) as input:
            csvReader = csv.reader(input)
            self.index = 0
            self.resampling = 0
            for row in csvReader:
                if row[0][1:].lower() == 'inspvasa':
                    try:
                        inspvasa
                    except NameError:
                        inspvasa = open('INSPVASA_{}.kml'.format(file[:-4]), 'w')
                        inspvasa.write(intro.format(row[0][1:].lower()))
                    inspvasa.write(self.inspvasa(row))
                elif row[0][1:].lower() == 'bestposa':
                    try:
                        bestposa
                    except NameError:
                        bestposa = open('BESTPOSA_{}.kml'.format(file[:-4]), 'w')
                        bestposa.write(intro.format(row[0][1:].lower()))
                    bestposa.write(self.bestposa(row))
                elif row[0][1:].lower() == 'bestgnssposa':
                    try:
                        bestgnssposa
                    except NameError:
                        bestgnssposa = open('BESTGNSSPOSA_{}.kml'.format(file[:-4]), 'w')
                        bestgnssposa.write(intro.format(row[0][1:].lower()))
                    bestgnssposa.write(self.bestposa(row))
                elif row[0][1:].lower() == 'inspvaxa':
                    try:
                        inspvaxa
                    except NameError:
                        inspvaxa = open('INSPVAXA_{}.kml'.format(file[:-4]), 'w')
                        inspvaxa.write(intro.format(row[0][1:].lower()))
                    inspvaxa.write(self.inspvaxa(row))
                elif row[0][1:].lower() == 'gpgga':
                    try:
                        gpgga
                    except NameError:
                        gpgga = open('GPGGA_{}.kml'.format(file[:-4]), 'w')
                        gpgga.write(intro.format(row[0][1:].lower()))
                    gpgga.write(self.gpgga(row))

            try:
                inspvasa
            except NameError:
                pass
            else:
                inspvasa.write(end)
                inspvasa.close()
            try:
                bestposa
            except NameError:
                pass
            else:
                bestposa.write(end)
                bestposa.close()
            try:
                bestgnssposa
            except NameError:
                pass
            else:
                bestgnssposa.write(end)
                bestgnssposa.close()
            try:
                inspvaxa
            except NameError:
                pass
            else:
                inspvaxa.write(end)
                inspvaxa.close()
            try:
                gpgga
            except NameError:
                pass
            else:
                gpgga.write(end)
                gpgga.close()

    def inspvasa(self,row):
        if self.index == self.resampling:
            # przeliczyć czas GPS na UTC?
            output = '		<Placemark>\n'\
                     '			<name>{}</name>\n'\
                     '			<Snippet maxLines="0"></Snippet>\n'\
                     '			<description><![CDATA[<B>Epoch: 10-K</B><BR><BR>\n'\
                     '			<TABLE border="1" width="100%" Align="center">\n'\
                     '				<TR ALIGN=RIGHT>\n'\
                     '				<TR ALIGN=RIGHT><TD ALIGN=LEFT>Time:</TD><TD>{}</TD><TD>{}</TD><TD></TD><TD></TD></TR>\n'\
                     '				<TR ALIGN=RIGHT><TD ALIGN=LEFT>Position:</TD><TD>{}</TD><TD>{}</TD><TD>{}</TD><TD>(DMS,m)</TD></TR>\n'\
                     '				<TR ALIGN=RIGHT><TD ALIGN=LEFT>Quality:</TD><TD>{}</TD><TD>Amb:</TD><TD>{}</TD><TD></TD></TR>\n'\
                     '			</TABLE>]]></description>\n'\
                     '			<TimeStamp><when>2015-08-29T16:11:49Z</when>\n'\
                     '			</TimeStamp>\n'\
                     '			<styleUrl>#Q4</styleUrl>\n'\
                     '			<Point>\n'\
                     '				<coordinates>{},{},{}</coordinates>\n'\
                     '			</Point>\n'\
                     '		</Placemark>\n'.format(row[3], row[3], row[1], row[4], row[5], row[6], 'NaN', row[13], row[5], row[4], row[6])

            return output
            self.index = 0
        else:
            self.index += 1

    def bestposa(self, row):
        if self.index == self.resampling:
            q = self.qualityCheck(row[16],row[17],row[10])
            # przeliczyć czas GPS na UTC?
            output = '		<Placemark>\n' \
                     '			<name>{}</name>\n' \
                     '			<Snippet maxLines="0"></Snippet>\n' \
                     '			<description><![CDATA[<B>Epoch: 10-K</B><BR><BR>\n' \
                     '			<TABLE border="1" width="100%" Align="center">\n' \
                     '			<TR ALIGN=RIGHT>\n' \
                     '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Time:</TD><TD>{}</TD><TD>{}</TD><TD></TD><TD></TD></TR>\n' \
                     '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Position:</TD><TD>{}</TD><TD>{}</TD><TD>{}</TD><TD>(DMS,m)</TD></TR>\n' \
                     '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Quality:</TD><TD>{}</TD><TD>Amb:</TD><TD>{}</TD><TD></TD></TR>\n' \
                     '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>nSats(tot,used):</TD><TD>{}</TD><TD>{}</TD><TD></TD><TD></TD></TR>\n'\
                     '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>SD(e,n,h):</TD><TD>{}</TD><TD>{}</TD><TD>{}</TD><TD>(m)</TD></TR>\n'\
                     '			</TABLE>]]></description>\n' \
                     '			<TimeStamp><when>2015-08-29T16:11:49Z</when>\n' \
                     '			</TimeStamp>\n' \
                     '			<styleUrl>#{}</styleUrl>\n' \
                     '			<Point>\n' \
                     '				<coordinates>{},{},{}</coordinates>\n' \
                     '			</Point>\n' \
                     '		</Placemark>\n'.format(row[6], row[6], row[5], row[11], row[12], row[13], q, row[10], row[22], row[23],row[16], row[17], row[18], q,
                                                     row[12], row[11], row[13])

            return output
            self.index = 0
        else:
            self.index += 1

    def inspvaxa(self, row):
        q = self.qualityCheck(row[21], row[22], row[10])
        # przeliczyć czas GPS na UTC?
        output = '		<Placemark>\n' \
                 '			<name>{}</name>\n' \
                 '			<Snippet maxLines="0"></Snippet>\n' \
                 '			<description><![CDATA[<B>Epoch: 10-K</B><BR><BR>\n' \
                 '			<TABLE border="1" width="100%" Align="center">\n' \
                 '			<TR ALIGN=RIGHT>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Time:</TD><TD>{}</TD><TD>{}</TD><TD></TD><TD></TD></TR>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Position:</TD><TD>{}</TD><TD>{}</TD><TD>{}</TD><TD>(DMS,m)</TD></TR>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Quality:</TD><TD>{}</TD><TD>Amb:</TD><TD>{}</TD><TD>{}</TD></TR>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>SD(e,n,h):</TD><TD>{}</TD><TD>{}</TD><TD>{}</TD><TD>(m)</TD></TR>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Extended sol:</TD><TD>{}</TD><TD></TD><TD></TD><TD>(m)</TD></TR>\n' \
                 '			</TABLE>]]></description>\n' \
                 '			<TimeStamp><when>2015-08-29T16:11:49Z</when>\n' \
                 '			</TimeStamp>\n' \
                 '			<styleUrl>#{}</styleUrl>\n' \
                 '			<Point>\n' \
                 '				<coordinates>{},{},{}</coordinates>\n' \
                 '			</Point>\n' \
                 '		</Placemark>\n'.format(row[6], row[6], row[5], row[11], row[12], row[13], q, row[10], row[9][row[9].index(';'):], row[21], row[22], row[23], row[30], q,
                                                 row[12], row[11], row[13])

        return output

    def gpgga(self, row):
        if row[6] == '4':
            q = 'Q1'
            status = 'FIXED'
        elif row[6] == '5':
            q = 'Q2'
            status = 'FLOAT'
        elif row[6] == '2':
            q = 'Q3'
            status = 'DGPS'
        else:
            q = 'Q4'
            status = 'Single'
        b = int(row[2][:2]) + float(row[2][2:])/60
        l = int(row[4][:3]) + float(row[4][3:])/60
        time = '{}:{}:{}'.format(row[1][:2], row[1][2:4], row[1][4:6])
        # przeliczyć czas GPS na UTC?
        output = '		<Placemark>\n' \
                 '			<name>{}</name>\n' \
                 '			<Snippet maxLines="0"></Snippet>\n' \
                 '			<description><![CDATA[<B>Epoch: 10-K</B><BR><BR>\n' \
                 '			<TABLE border="1" width="100%" Align="center">\n' \
                 '			<TR ALIGN=RIGHT>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Time:</TD><TD>{}</TD><TD></TD><TD></TD><TD></TD></TR>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Position:</TD><TD>{}</TD><TD>{}</TD><TD>{}</TD><TD>(DMS,m)</TD></TR>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>Quality:</TD><TD>{}</TD><TD>Amb:</TD><TD>{}</TD><TD></TD></TR>\n' \
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>nSats(used):</TD><TD>{}</TD><TD></TD><TD></TD><TD></TD></TR>\n'\
                 '			<TR ALIGN=RIGHT><TD ALIGN=LEFT>SD(e,n,h):</TD><TD></TD><TD></TD><TD></TD><TD>(m)</TD></TR>\n'\
                 '			</TABLE>]]></description>\n' \
                 '			<TimeStamp><when>2015-08-29T{}Z</when>\n' \
                 '			</TimeStamp>\n' \
                 '			<styleUrl>#{}</styleUrl>\n' \
                 '			<Point>\n' \
                 '				<coordinates>{},{},{}</coordinates>\n' \
                 '			</Point>\n' \
                 '		</Placemark>\n'.format(row[1], row[1], b, b, row[9], q, status, row[7], time, q, l, b, row[13])

        return output

    def qualityCheck(self, sdx, sdy, status):
        sd = sqrt(float(sdx) ** 2 + float(sdy) ** 2)
        if sd < 0.05:
            if status == 'NARROW_INT':
                q = 'Q1'
            else:
                q = 'Q2'
        elif sd < 0.20:
            if status == 'NARROW_INT' or status == 'INS_RTKFIXED':
                q = 'Q3'
            else:
                q = 'Q4'
        elif sd < 0.50:
            q = 'Q5'
        else:
            q = 'Q6'

        return q

# Program
test = INS2KML()
for i in test.find_file():
    test.create_kml(i)
